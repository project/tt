<?php
/**
 * Render a field as a numeric value
 *
 * Definition terms:
 * - float: If true this field contains a decimal value. If unset this field
 *          will be assumed to be integer.
 *
 * @ingroup views_field_handlers
 */
class tt_handler_field_total extends views_handler_field_numeric {
  function query() {
    $this->add_additional_fields();
  }

  function render($values) { 
    $nid = $values->nid;

    $sql = 'SELECT SUM(hours) as total FROM {tt} WHERE nid = "%d"';
    $values = array($nid);

    // TODO: Check for argument
    dsm($this->view);

    $total = db_result(db_query($sql, $values));
      
    return sprintf("%.2f", $total);
  }
}
