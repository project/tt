<?php

/**
 * @file 
 * Handles integration with views
 */

/**
 * Implementation of hook_views_data()
 */
function tt_views_data() {
  $data['tt']['table']['group'] = t('Time Tracker');

  $data['tt']['table']['base'] = array(
    'field' => 'ttid',
    'title' => t('Time reports'),
    'help' => t('List all reports associated with nodes'),
    'weight' => 10,
  );

  $data['tt']['table']['join'] = array(
    'node' => array(
      'left_field' => 'nid',
      'field' => 'nid',
    ),
    'users' => array(
      'left_field' => 'uid',
      'field' => 'uid',
    ),
  );

  $data['tt']['hours'] = array(
    'title' => t('Reported hours'),
    'help' => t('Hours reported'),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
      'allow empty' => TRUE,
    ),
    'field' => array(
      'handler' => 'tt_handler_field_hours',
    ),
  );

  $data['tt']['uid'] = array(
    'title' => t('User reporting hours'),
    'help' => t('Filter/argument for selecting reports from user'),
    'argument' => array(
      'handler' => 'views_handler_argument_user_uid',
      'name field' => 'uid',
      'name table' => 'users',
      'numeric' => TRUE,
      'validate type' => 'uid',
    ), 
    'relationship' => array(
      'base' => 'users',
      'base field' => 'uid',
    ),
  );

  $data['tt']['nid'] = array(
    'title' => t('Node'),
    'help' => t('Node for which time was reported'),
    'relationship' => array(
      'base' => 'node',
      'base field' => 'nid',
    ),
  );

  $data['tt']['timestamp'] = array(
    'title' => t('Report date'), // The item it appears as on the UI,
    'help' => t('The date the report was issued.'), // The help that appears on the UI,
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );

  $data['node']['tt_total'] = array(
    'title' => t('Total hours worked'),
    'help' => t('The sum of all hours worked on a node'),
    'field' => array(
      'handler' => 'tt_handler_field_total',
      'label' => t('Total hours tracked'),
    ),
  );

  $data['node']['table']['join'] = array(
    'tt' => array(
      'field' => 'nid',
      'left_field' => 'nid',
    ),
  );
  $data['users']['table']['join'] = array(
    'tt' => array(
      'field' => 'uid',
      'left_field' => 'uid',
    ),
  );

  return $data;
}

/**
 * Implementation of hook_handlers()
 */
function tt_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'tt') . '/views', 
    ),
    'handlers' => array(
      'tt_handler_field_total' => array(
        'parent' => 'views_handler_field_numeric',
        'file' => 'tt_handler_field_total.inc',
      ),
      'tt_handler_field_hours' => array(
        'parent' => 'views_handler_field_numeric',
        'file' => 'tt_handler_field_hours.inc',
      ),
    ),
  );
        
}
