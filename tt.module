<?php

/**
 * @file 
 * The main module file for Time Tracker, a simple time tracking
 * module
 */

// TODO: date formats

/**
 * Implementation of hook_block()
 */
function tt_block($op = 'list', $delta = 0, $edit = array()) {
  switch ($op) {
    case 'list':
    $blocks = array(
      0 => array('info' => t('Time Tracker'), 'cache' => BLOCK_NO_CACHE)
    );
    return $blocks;

    case 'view':
    switch ($delta) {
      case 0:
      $block = array(
        'subject' => t('Time Tracker'),
        'content' => tt_block_content(),
      );
    }
    return $block;
  }
}

/**
 * Renders block content for time tracking
 * @param   
 * @return  
 */
function tt_block_content() {
  global $user;
  $content = '';
  if (($node = menu_get_object()) && tt_is_trackeable($node)) {
    $content = drupal_get_form('tt_track', $node);
    $reports = tt_load($node);
    $content .= theme('tt_table_node_user', $reports, $user);
  }

  return $content;
}

/**
 * This function checks settings for node type, to see if its trackeable
 * @param $node 
 * @return boolean
 */
function tt_is_trackeable($node) {
  if (!variable_get('tt_' . $node->type, FALSE)) {
    return FALSE;
  }
  return user_access('report time on nodes');
}

/**
 * The form for inserting a new time report
 * @param $node
 * @return  
 */
function tt_track($form_state, $node) {
  global $user;
  
  $form = array();

  $form['#uid'] = $user->uid;
  $form['#nid'] = $node->nid;

  $form['timestamp'] = array(
    '#type' => 'textfield',
    '#description' => t('YYYY-MM-DD'),
    '#default_value' => format_date(time(), 'custom', 'Y-m-d'),
    '#required' => TRUE,
    '#size' => 10,
  );

  $form['hours'] = array(
    '#type' => 'textfield',
    '#description' => t('hh.xx (decimal) or hh:mm'),
    '#field_suffix' => t('Hours'),
    '#required' => TRUE,
    '#size' => 4,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Report time'),
  );

  return $form;
  
}

/**
 * Validates the report
 * @param   
 * @return  
 */
function tt_track_validate($form, &$form_state) {

  $tz_correction = variable_get('date_default_timezone', 0);
  $timestamp = strtotime($form_state['values']['timestamp']);
  if ($timestamp === FALSE || format_date($timestamp, 'custom', 'Y-m-d') != $form_state['values']['timestamp']) {
    form_set_error('timestamp', t('Invalid date format'));
  }

  $form_state['values']['timestamp'] = $timestamp - $tz_correction;

  if (strpos($form_state['values']['hours'], ':') !== FALSE) {
    // Parse hh:mm format
    $hour_parts = explode(':', $form_state['values']['hours']);
    if (count($hour_parts) !== 2 || !is_numeric($hour_parts[0]) || !is_numeric($hour_parts[1])) {
      form_set_error('hours', t('Please type the value for hours in a valid format.'));
    }
    else {
      $form_state['values']['hours'] = (float) ($hour_parts[0] + $hour_parts[1] / 60);
    }
  }

  if (!is_numeric($form_state['values']['hours'])) {
    form_set_error('hours', t('Please type the value for hours in a valid format.'));
  }
}

/**
 * Saves the new report
 * @param   
 * @return  
 */
function tt_track_submit($form, &$form_state) {
  $data = array(
    'uid' => $form['#uid'],
    'nid' => $form['#nid'],
    'timestamp' => (int) $form_state['values']['timestamp'],
    'hours' => (float) $form_state['values']['hours'],
  );

  if (drupal_write_record('tt', $data)) {
    drupal_set_message(t('Time report successful'));
  }
  else {
    drupal_set_message(t('Error saving time report'), 'error');
  }
}

/**
 * Implementation of hook_init()
 */
function tt_init() {
  drupal_add_css(drupal_get_path('module', 'tt') . '/tt.css');
}

/**
 * Implementation of hook_theme()
 */
function tt_theme() {
  return array(
    'tt_table_node_user' => array(
      'arguments' => array('reports' => NULL, 'user' => NULL),
    ),
  );
}

/**
 * Loads all reports for node (optionally filtering by user)
 * @param   
 * @return  
 */
function tt_load($node, $uid = NULL) {
  $sql = 'SELECT * FROM {tt} WHERE nid = %d';
  $values = array($node->nid);

  if ($uid) {
    $sql .= ' AND uid = %d';
    $values[] = $uid;
  }

  $sql .= ' ORDER BY timestamp DESC';

  $result = db_query($sql, $values);
  $reports = array();
  while ($item = db_fetch_object($result)) {
    $reports[] = $item;
  }

  return $reports;
}

/**
 * Renders table with all reports for a node
 * @param $reports
 * @param $user - If specified, highlight user on table
 * @return  
 */
function theme_tt_table_node_user($reports, $user = NULL) {
  $header = array('', '', '', '');
  $header = array();

  $rows = array();
  $total = 0;
  foreach ($reports as $report) {
    $timestamp = array('data' => format_date($report->timestamp, 'custom', 'Y-m-d'), 'class' => 'timestamp');

    $acc = user_load($report->uid);
    $name = theme('username', $acc);

    $hours = sprintf("%.2f", $report->hours);
    $total += $report->hours;

    $delete_item = menu_get_item('tt/delete/' . $report->ttid);
    $delete = ($delete_item && $delete_item['access']) ? l(t('delete'), $delete_item['href'], array('query' => array('destination' => $_GET['q']))) : '';

    $class = ($user->uid == $acc->uid) ? 'current-user' : '';
    $rows[] = array('data' => array($timestamp, $name, $hours, $delete), 'class' => $class);
  }
  
  $total_label = array('data' => t('Total'), 'colspan' => 2);
  $total_value = sprintf("%.2f", $total);
  $rows[] = array('data' => array($total_label, $total_value, ''), 'class' => 'total');

  return theme('table', $header, $rows, array('class' => 'tt-table'));
}

/**
 * Implementation of hook_perm()
 */
function tt_perm() {
  return array('report time on nodes', 'delete own time reports', 'administer time reports');
}

/**
 * Implementation of hook_form_alter()
 */
function tt_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id == 'node_type_form') {
    $form['tt'] = array(
      '#type' => 'fieldset',
      '#title' => t('Time Tracker'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE, 
    );
    $form['tt']['tt'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enable time reporting'),
      '#default_value' => variable_get('tt_book', FALSE),
    );
    
  }
}

/**
 * Implementation of hook_menu()
 */
function tt_menu() {
  $items = array();

  $items['tt/delete/%'] = array(
    'title' => t('Delete reported time'),
    'page callback' => 'tt_delete',
    'page arguments' => array(2),
    'access callback' => 'tt_delete_access',
    'access arguments' => array(2),
    'type' => MENU_CALLBACK,
  );

  return $items;
}

/**
 * Simply delete a report. Be careful, no checks in here, no turning back
 * @param $ttid
 */
function tt_delete($ttid) {
  db_query('DELETE FROM {tt} WHERE ttid = %d', $ttid);
  drupal_goto();
}

/**
 * Checks access for deleting a time report
 * @param $ttid
 * @return TRUE or FALSE
 */
function tt_delete_access($ttid) {
  global $user;

  if (user_access('administer time reports')) {
    return TRUE;
  }
  $uid = db_result(db_query('SELECT uid FROM {tt} WHERE ttid = %d', $ttid));

  if ($uid == $user->uid && user_access('delete own time reports')) {
    return TRUE;
  }

  return FALSE;
}


/**
 * Implementation of hook_views_api()
 */
function tt_views_api() {
  return array(
    'api' => 2,
    'path' => drupal_get_path('module', 'tt') . '/views', 
  );
}

